﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dicom.Media;
using Dicom;

namespace DVM_app
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            try
            {               

                var action = "read";
                var path = "e:/Dokumenty/TMS/TMS/Fiserova 2/FISEROVA_ALENA_19680318_6853181676_679eb763/BinData/DICOM/";

                if (action == "read")
                {
                    path = Path.Combine(path, "DICOMDIR");

                    if (!File.Exists(path))
                    {
                        Console.WriteLine("DICOMDIR file not found: {0}", path);
                        return;
                    }

                    ReadMedia(path);
                    return;
                }

                WriteMedia(path);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {

            }
        }

        private static void WriteMedia(string path)
        {
            var dicomDirPath = Path.Combine(path, "DICOMDIR");

            var dirInfo = new DirectoryInfo(path);

            var dicomDir = new DicomDirectory();
            foreach (var file in dirInfo.GetFiles("*.*", SearchOption.AllDirectories))
            {
                var dicomFile = Dicom.DicomFile.Open(file.FullName);

                dicomDir.AddFile(dicomFile, String.Format(@"000001\{0}", file.Name));
            }

            dicomDir.Save(dicomDirPath);
        }

        private static void ReadMedia(string fileName)
        {
            var dicomDirectory = DicomDirectory.Open(fileName);

            foreach (var patientRecord in dicomDirectory.RootDirectoryRecordCollection)
            {
                Console.WriteLine(
                    "Patient: {0} ({1})",
                    patientRecord.Get<string>(DicomTag.PatientName),
                    patientRecord.Get<string>(DicomTag.PatientID));

                foreach (var studyRecord in patientRecord.LowerLevelDirectoryRecordCollection)
                {
                    Console.WriteLine("\tStudy: {0}", studyRecord.Get<string>(DicomTag.StudyInstanceUID));

                    foreach (var seriesRecord in studyRecord.LowerLevelDirectoryRecordCollection)
                    {
                        Console.WriteLine("\t\tSeries: {0}", seriesRecord.Get<string>(DicomTag.SeriesInstanceUID));

                        foreach (var imageRecord in seriesRecord.LowerLevelDirectoryRecordCollection)
                        {
                            Console.WriteLine(
                                "\t\t\tImage: {0} [{1}]",
                                imageRecord.Get<string>(DicomTag.ReferencedSOPInstanceUIDInFile),
                                imageRecord.Get<string>(Dicom.DicomTag.ReferencedFileID));
                        }
                    }
                }
            }
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Usage: Dicom.Media.exe read|write <directory>");
        }
            
        }
        
}
