﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using MIConvexHull;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Globalization;

namespace DVM_app
{
    public enum ImageBitsPerPixel { Eight, Sixteen, TwentyFour };
    public enum ViewSettings { Zoom1_1, ZoomToFit };

    public partial class Form1 : Form
    {
        const string pixSpacTag = "00280030";
        public DicomDecoder dd;
        OpenFileDialog ofd;
        List<byte> pixels8;
        List<ushort> pixels16;
        List<ushort> pixels16All;
        List<Point> points;
        List<byte> pixels24;
        int imageWidth;
        int imageHeight;
        int bitDepth;
        int samplesPerPixel;
        bool imageOpened;
        bool folderOpened;
        double winCentre;
        double winWidth;
        bool signedImage;
        int maxPixelValue;
        int minPixelValue;
        double pixelSpac;
        string[] files;
        int[] locations;
        int minCord;
        int maxCord;

        public Form1()
        {
            InitializeComponent();
            dd = new DicomDecoder();
            pixels8 = new List<byte>();
            pixels24 = new List<byte>();
            pixels16 = new List<ushort>();            
            imageOpened = false;
            signedImage = false;
            folderOpened = false;
            maxPixelValue = 0;
            minPixelValue = 65535;
            pixelSpac = 1;
        }

        private void InitVariables()
        {
            pixels16All = new List<ushort>();
            points = new List<Point>();
            folderOpened = true;
            minCord = Int32.MaxValue;
            maxCord = 0;
        }

        private void bnOpen_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = Properties.Settings.Default.folder;
            DialogResult result = fbd.ShowDialog();

            if ((result == DialogResult.OK) && (fbd.SelectedPath.Length > 0))
            {
                if (!String.IsNullOrEmpty(Properties.Settings.Default.folder))
                {
                    Properties.Settings.Default.folder = fbd.SelectedPath;
                }
                // saving last opened folder as default
                Properties.Settings.Default.folder = fbd.SelectedPath;
                Properties.Settings.Default.Save();

                InitVariables();
                files = Directory.GetFiles(fbd.SelectedPath);
                int fileCount = files.Length;
                locations = new int[fileCount];
                System.Windows.Forms.MessageBox.Show("Nalezeno: " + fileCount.ToString() + " souborů", "Oznámení");

                for (int i = 0; i < fileCount; i++)
                {
                    string fileName = files[i];
                    string saveFileName = fileName.Substring(fbd.SelectedPath.Length + 1);
                    Cursor = Cursors.WaitCursor;
                    ReadAndDisplayDicomFile(fileName, saveFileName);
                    LoadTriggers();
                    locations[i] = dd.offset;
                    imageOpened = true;
                }
                Cursor = Cursors.Default;
            

                string s1, s11, s12;
                List<string> str = dd.dicomInfo;
                for (int i = 1; i < str.Count(); i++)
                {
                    s1 = str[i];
                    ExtractStrings(s1, out s12, out s11);
                    if (s11.CompareTo(pixSpacTag) == 0)
                    {
                        s12 = s12.Substring(0, s12.IndexOf("\\"));
                        pixelSpac = Double.Parse(s12, new CultureInfo("en-US"));
                        break;
                    }
                }

                ReadAndDisplayDicomFile(ofd.FileName, ofd.SafeFileName);
                FindConvexHull();
            
            }
            fbd.Dispose();
            folderOpened = false;
        }

        private void ReadAndDisplayDicomFile(string fileName, string fileNameOnly)
        {
            dd.DicomFileName = fileName;
            
            TypeOfDicomFile typeOfDicomFile = dd.typeofDicomFile;

            if (typeOfDicomFile == TypeOfDicomFile.Dicom3File ||
                typeOfDicomFile == TypeOfDicomFile.DicomOldTypeFile)
            {
                imageWidth = dd.width;
                imageHeight = dd.height;
                bitDepth = dd.bitsAllocated;
                winCentre = dd.windowCentre;
                winWidth = dd.windowWidth;
                samplesPerPixel = dd.samplesPerPixel;
                signedImage = dd.signedImage;

                label1.Visible = true;
                label2.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
                bnSave.Enabled = true;
                bnTags.Enabled = true;
                //bnResetWL.Enabled = true;
                label2.Text = imageWidth.ToString() + " X " + imageHeight.ToString();
                if (samplesPerPixel == 1)
                    label4.Text = bitDepth.ToString() + " bit";
                else
                    label4.Text = bitDepth.ToString() + " bit, " + samplesPerPixel +
                        " samples per pixel";

                //imagePanelControl.NewImage = true;
                Text = "DICOM Image Viewer: " + fileNameOnly;

                if (samplesPerPixel == 1 && bitDepth == 8)
                {
                    pixels8.Clear();
                    pixels16.Clear();
                    pixels24.Clear();
                    dd.GetPixels8(ref pixels8);

                    // This is primarily for debugging purposes, 
                    //  to view the pixel values as ascii data.
                    //if (true)
                    //{
                    //    System.IO.StreamWriter file = new System.IO.StreamWriter(
                    //               "C:\\imageSigned.txt");

                    //    for (int ik = 0; ik < pixels8.Count; ++ik)
                    //        file.Write(pixels8[ik] + "  ");

                    //    file.Close();
                    //}

                    minPixelValue = pixels8.Min();
                    maxPixelValue = pixels8.Max();

                    // Bug fix dated 24 Aug 2013 - for proper window/level of signed images
                    // Thanks to Matias Montroull from Argentina for pointing this out.
                    if (dd.signedImage)
                    {
                        winCentre -= char.MinValue;
                    }

                    if (Math.Abs(winWidth) < 0.001)
                    {
                        winWidth = maxPixelValue - minPixelValue;
                    }

                    if ((winCentre == 0) ||
                        (minPixelValue > winCentre) || (maxPixelValue < winCentre))
                    {
                        winCentre = (maxPixelValue + minPixelValue) / 2;
                    }

                    /*imagePanelControl.SetParameters(ref pixels8, imageWidth, imageHeight,
                        winWidth, winCentre, samplesPerPixel, true, this);*/
                }

                if (samplesPerPixel == 1 && bitDepth == 16)
                {
                    pixels16.Clear();
                    pixels8.Clear();
                    pixels24.Clear();
                    dd.GetPixels16(ref pixels16);

                    // This is primarily for debugging purposes, 
                    //  to view the pixel values as ascii data.
                    //if (true)
                    //{
                    //    System.IO.StreamWriter file = new System.IO.StreamWriter(
                    //               "C:\\imageSigned.txt");

                    //    for (int ik = 0; ik < pixels16.Count; ++ik)
                    //        file.Write(pixels16[ik] + "  ");

                    //    file.Close();
                    //}

                    minPixelValue = pixels16.Min();
                    maxPixelValue = pixels16.Max();

                    // Bug fix dated 24 Aug 2013 - for proper window/level of signed images
                    // Thanks to Matias Montroull from Argentina for pointing this out.
                    if (dd.signedImage)
                    {
                        winCentre -= short.MinValue;
                    }

                    if (Math.Abs(winWidth) < 0.001)
                    {
                        winWidth = maxPixelValue - minPixelValue;
                    }

                    if ((winCentre == 0) ||
                        (minPixelValue > winCentre) || (maxPixelValue < winCentre))
                    {
                        winCentre = (maxPixelValue + minPixelValue) / 2;
                    }

                    if (!folderOpened)
                    {
                        imagePanelControl.Signed16Image = dd.signedImage;

                        imagePanelControl.SetParameters(ref pixels16, imageWidth, imageHeight,
                            winWidth, winCentre, true, this);
                    }
                }

                if (samplesPerPixel == 3 && bitDepth == 8)
                {
                    // This is an RGB colour image
                    pixels8.Clear();
                    pixels16.Clear();
                    pixels24.Clear();
                    dd.GetPixels24(ref pixels24);

                    // This code segment is primarily for debugging purposes, 
                    //    to view the pixel values as ascii data.
                    //if (true)
                    //{
                    //    System.IO.StreamWriter file = new System.IO.StreamWriter(
                    //                      "C:\\image24.txt");

                    //    for (int ik = 0; ik < pixels24.Count; ++ik)
                    //        file.Write(pixels24[ik] + "  ");

                    //    file.Close();
                    //}

                    /*imagePanelControl.SetParameters(ref pixels24, imageWidth, imageHeight,
                        winWidth, winCentre, samplesPerPixel, true, this);*/
                }
            }
            else
            {
                if (typeOfDicomFile == TypeOfDicomFile.DicomUnknownTransferSyntax)
                {
                    MessageBox.Show("Sorry, I can't read a DICOM file with this Transfer Syntax.",
                        "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Sorry, I can't open this file. " +
                        "This file does not appear to contain a DICOM image.",
                        "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Text = "DICOM Image Viewer: ";
                // Show a plain grayscale image instead
                pixels8.Clear();
                pixels16.Clear();
                pixels24.Clear();
                samplesPerPixel = 1;

                imageWidth = imagePanelControl.Width - 25;   // 25 is a magic number
                imageHeight = imagePanelControl.Height - 25; // Same magic number
                int iNoPix = imageWidth * imageHeight;

                for (int i = 0; i < iNoPix; ++i)
                {
                    pixels8.Add(240);// 240 is the grayvalue corresponding to the Control colour
                }
                winWidth = 256;
                winCentre = 127;
                /*imagePanelControl.SetParameters(ref pixels8, imageWidth, imageHeight,
                    winWidth, winCentre, samplesPerPixel, true, this);
                imagePanelControl.Invalidate();*/
                label1.Visible = false;
                label2.Visible = false;
                label3.Visible = false;
                label4.Visible = false;
                bnSave.Enabled = false;
                bnTags.Enabled = false;
                
            }
        }

        private void LoadTriggers()
        {
            int x, y;
            for (int i = 0; i < pixels16.Count; i++)
            {
                if (pixels16.ElementAt(i) != 0)
                {
                    x = (i % dd.height) + 1;
                    y = (i / dd.height) + 1;
                    points.Add(new Point(x, y));
                    pixels16All.Add(pixels16.ElementAt(i));
                }
            }
        }

        private void FindConvexHull() {
            
            var vertices = new Vertex[points.Count];
            for (int i = 0; i < points.Count; i++) 
            {
                vertices[i] = new Vertex(points.ElementAt(i).X, points.ElementAt(i).Y);
            }
            FindMinAndMax(vertices);

            imagePanelControl.Signed16Image = dd.signedImage;

            dd.summary = imagePanelControl.SetParameters(ref pixels16, imageWidth, imageHeight,
                winWidth, winCentre, true, this, vertices, pixels16All, pixelSpac);

            label6.Text = dd.summary.ToString() + " mm\u00b2";
        }

        private void FindMinAndMax(Vertex[] vertices)
        {
            foreach (Vertex v in vertices)
            {
                double pixel = (v.Position.Last() * dd.width) + v.Position.First();
                if (pixel < minCord)
                    minCord = (int)pixel;
                else if (pixel > maxCord)
                    maxCord = (int)pixel;
            }
        }

        public void ExtractStrings(string s1, out string s2, out string s11)
        {
            s11 = s1.Substring(0, 8);
            int ind = s1.IndexOf(":");
            s2 = s1.Substring(ind + 1);
        }

        private void bnOpenFile_Click(object sender, EventArgs e)
        {
            ofd = new OpenFileDialog();
            ofd.Filter = "All DICOM Files(*.*)|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (ofd.FileName.Length > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    ReadAndDisplayDicomFile(ofd.FileName, ofd.SafeFileName);
                    imageOpened = true;
                    this.bnOpen.Enabled = true;
                    Cursor = Cursors.Default;
                }
                ofd.Dispose();
            }
        }

        private void bnTags_Click(object sender, EventArgs e)
        {
            if (imageOpened == true)
            {
                List<string> str = dd.dicomInfo;

                DicomTagsForm dtg = new DicomTagsForm();
                dtg.SetString(ref str);
                dtg.ShowDialog();

                imagePanelControl.Invalidate();
            }
            else
                MessageBox.Show("Load a DICOM file before viewing tags!", "Information",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bnSave_Click(object sender, EventArgs e)
        {
            if (imageOpened == true)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "PNG Files(*.png)|*.png";

                if (sfd.ShowDialog() == DialogResult.OK)
                    imagePanelControl.SaveImage(sfd.FileName);
            }
            else
                MessageBox.Show("Load a DICOM file before saving!", "Information", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

            imagePanelControl.Invalidate();
        }

        /*private void bnSave_Click(object sender, EventArgs e)
        {
            if (imageOpened == true)
            {
                //SaveFileDialog sfd = new SaveFileDialog();
                //sfd.Filter = "DCM Files(*.dcm)|*.dcm";

                //if (sfd.ShowDialog() == DialogResult.OK)

                //for (int i = 0; i < files.Count(); i++)
                //{
                    //string fileName = files[i];
                    string fileName = files.Last();
                    //string saveFileName = fileName.Substring(fbd.SelectedPath.Length + 1);
                    Cursor = Cursors.WaitCursor;
                    //ReadAndDisplayDicomFile(fileName, saveFileName);
                    imagePanelControl.SaveImage(dd, fileName, minCord, maxCord, locations.Last());//locations[i]);
                    Cursor = Cursors.Default;
                //}

                //imagePanelControl.SaveImage(dd, minCord, maxCord);
            }
            else
                MessageBox.Show("Load a DICOM file before saving!", "Information",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

            imagePanelControl.Invalidate();
        }*/

        private void MainForm_Load(object sender, EventArgs e)
        {
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            pixels8.Clear();
            pixels16.Clear();
            //if (imagePanelControl != null) imagePanelControl.Dispose();
        }

        /*public void UpdateWindowLevel(int winWidth, int winCentre, ImageBitsPerPixel bpp)
        {
            int winMin = Convert.ToInt32(winCentre - 0.5 * winWidth);
            int winMax = winMin + winWidth;
            this.windowLevelControl.SetWindowWidthCentre(winMin, winMax, winWidth, winCentre, bpp, signedImage);
        }*/

        public void UpdateWindowLevel(int winWidth, int winCentre, ImageBitsPerPixel bpp)
        {
            int winMin = Convert.ToInt32(winCentre - 0.5 * winWidth);
            int winMax = winMin + winWidth;
            //this.windowLevelControl.SetWindowWidthCentre(winMin, winMax, winWidth, winCentre, bpp, signedImage);
        }

        private void viewSettingsCheckedChanged(object sender, EventArgs e)
        {
            /*
            if (rbZoom1_1.Checked)
            {
                imagePanelControl.viewSettings = ViewSettings.Zoom1_1;
            }
            else
            {
               /imagePanelControl.viewSettings = ViewSettings.ZoomToFit;
            }
            */
            /*imagePanelControl.viewSettingsChanged = true;
            imagePanelControl.Invalidate();*/
        }
    }
}
