﻿using Dicom;
using Dicom.Imaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVM_app
{
    class Openfile
    {
        public static void ReadFile(string filename)
        {
            var image = new DicomImage(filename);
            var pixels = image.RenderImage().Pixels;

            using (FileStream fs = File.OpenRead(filename))
            {
                fs.Seek(128, SeekOrigin.Begin);
                byte test = (byte)fs.ReadByte();
                if (!((byte)fs.ReadByte() != (byte)'D' ||
                      (byte)fs.ReadByte() != (byte)'I' ||
                      (byte)fs.ReadByte() != (byte)'C' ||
                      (byte)fs.ReadByte() != (byte)'M'))
                {
                    Console.WriteLine("Not a DCM");
                    return;
                }
                //BinaryReader reader = new BinaryReader(fs);
                
                fs.Seek(2192, SeekOrigin.Begin);
                BinaryReader reader = new BinaryReader(fs);
                
                ushort g;
                //ushort e;
                do
                {
                    g = reader.ReadUInt16();
                    //e = reader.ReadUInt16();

                    //string vr = new string(reader.ReadChars(2));
                    //long length;
                    //if (vr.Equals("AE") || vr.Equals("AS") || vr.Equals("AT")
                    //    || vr.Equals("CS") || vr.Equals("DA") || vr.Equals("DS")
                    //    || vr.Equals("DT") || vr.Equals("FL") || vr.Equals("FD")
                    //    || vr.Equals("IS") || vr.Equals("LO") || vr.Equals("PN")
                    //    || vr.Equals("SH") || vr.Equals("SL") || vr.Equals("SS")
                    //    || vr.Equals("ST") || vr.Equals("TM") || vr.Equals("UI")
                    //    || vr.Equals("UL") || vr.Equals("US"))
                    //    length = reader.ReadUInt16();
                    //else
                    //{
                    //    // Read the reserved byte
                    //    reader.ReadUInt16();
                    //    length = reader.ReadUInt32();
                    //}

                    //byte[] val = reader.ReadBytes((int)length);

                } while (g == 2);

                fs.Close();
            }

            return;
        }


    }
}
