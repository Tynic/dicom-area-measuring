# README #

DICOM viewer project for measuring area of the given points. Points are patient's responses durng TMS (Transcranial magnetic stimulation) examination.

Please see [**ENGLISH DOCUMENTATION**](https://drive.google.com/open?id=0B-QpyKFdHxqwdmltaXhpU2tqVWs) for deep problem description & analysis. 

[**TEST DATA**](https://drive.google.com/open?id=0B-QpyKFdHxqwWHQ1Ni1HR2pZcHM)